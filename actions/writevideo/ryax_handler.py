# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import json
import os

import cv2

IMAGE_EXT = [
    ".bmp",
    ".pbm",
    ".pgm",
    ".ppm",
    ".sr",
    ".ras",
    ".jpeg",
    ".jpg",
    ".jpe",
    ".jp2",
    ".tiff",
    ".tif",
    ".png",
]


def parse_dir(dirname):
    fileList = []
    info_json = ""
    for filename in os.listdir(dirname):
        if os.path.isfile(dirname + "/" + filename) is True:
            if filename == "info.json":
                info_json = os.path.join(dirname, filename)
                print(f"File info.json found at {info_json}")
            else:
                _, ext = os.path.splitext(filename)
                if ext in IMAGE_EXT:
                    fileList.append(dirname + "/" + filename)
    if not os.path.isfile(info_json):
        print("ERROR the file info.json could not be found, check cut-video function!")
    return (fileList, info_json)


def prepare_output(info, output_path):
    size = (info["width"], info["height"])
    print(f"Using filename {os.path.basename(info['filename'])}")
    video_path = os.path.join(
        output_path, f"{os.path.basename(info['filename'])}-tagged{info['ext']}"
    )
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    return cv2.VideoWriter(video_path, fourcc, info["fps"], size), video_path


def handle(req):
    print(f"Req ========> {req}")
    dirname = req["frames"]
    frameList, info_json = parse_dir(dirname)
    output_path = os.path.join("/tmp", "RESULT_VIDEO")
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    if frameList and info_json:
        frameList.sort()
        info_txt = open(info_json)
        info = json.loads(info_txt.read())
        print(f"Opened {info_json} for reading")
        if info["frame_count"] == len(frameList):
            print(
                f"Frames expected {len(frameList)}, frames counted {info['frame_count']}!"
            )
            # Prepare video writer
            out, video_path = prepare_output(info, output_path)
            print(f"Writing tagged video to {video_path}!")
            for f in frameList:
                # Write video
                image_np = cv2.imread(f)
                out.write(image_np)
            out.release()
            if os.path.isfile(video_path):
                video_info = os.stat(video_path)
                if video_info.st_size > 0:
                    print(f"Created file {video_path}, size is {video_info.st_size}")
                else:
                    print(
                        f"Something went wrong file {video_path}, is zero bytes long!"
                    )
            else:
                print(f"File {video_path} does not exists, cv2.VideoWriter failed!")
            return {"video": video_path}
        else:
            print(
                f"ERROR Frames expected {len(frameList)}, frames counted {info['frame_count']}!"
            )
    else:
        print(f"ERROR cannot find frameList or info_json in directory {dirname}")
