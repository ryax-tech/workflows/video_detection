## WriteVideo

WriteVideo will write a video from a frames.

To write the video, WriteVideo need numbered frames and a file named info.json containing video infos (see example). If you have cut your video with [VideoFrameByFrame](../videoframebyframe) you don't have to worry about that.

**Inputs:**
> frames
- Directory containing all numbered frames and video infos.

Outputs:
> video
- Path to the assembled video

&nbsp;

**info.json** example:
> {"fps": 25.0, "width": 1920, "height": 1080, "fourcc": 828601953, "frame_count": 393, "ext": ".mp4", "filename": "Video"}