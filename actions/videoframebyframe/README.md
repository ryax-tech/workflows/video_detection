# VideoFrameByFrame

VideoFrameByFrame will cut in frames a video and return directory that contains frames.

All frames will be numbered and a JSON file will be created this one will contain the informations of the video (Filename, FPS, [FourCC](https://www.fourcc.org/fourcc.php), Frames, ...).

**Input:**
> video
- Video that will be cut.

**Output:**
> frames
- Directory containing all numbered frames and video infos.

&nbsp;

**info.json** example:
> {"fps": 25.0, "width": 1920, "height": 1080, "fourcc": 828601953, "frame_count": 393, "ext": ".mp4", "filename": "Video"}