# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import json
import os

import cv2


def save_video_info(cap, path, filename, frames):
    info = {}
    name, ext = os.path.splitext(filename)
    info["filename"] = name
    info["ext"] = ext
    info["fps"] = cap.get(cv2.CAP_PROP_FPS)
    info["frame_count"] = frames
    info["width"] = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    info["height"] = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    info["fourcc"] = int(cap.get(cv2.CAP_PROP_FOURCC))
    if info["fourcc"] == 0:
        info["fourcc"] = cv2.VideoWriter_fourcc(*"mp4v")
        info["ext"] = ".mp4"
    info_json = json.dumps(info)
    f = open(path + "/info.json", "w")
    f.write(info_json)
    f.close()
    return info


def handle(req):
    print(f"Request ====> {req}")
    # Video path
    output_dir = "/tmp/output"
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    # Find video filename
    filename = req["video"]
    cap = cv2.VideoCapture(filename)
    idx = 0
    # Read video and save each frame
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        image_path = os.path.join(output_dir + "/" + str(idx).zfill(10) + ".jpg")
        cv2.imwrite(image_path, frame)
        idx += 1
    # Saving video infos
    save_video_info(cap, output_dir, filename, idx)
    return {"frames": output_dir}
