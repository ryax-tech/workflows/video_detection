# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import os
import shutil
import tarfile

import cv2
import numpy as np
import tensorflow as tf
import urllib3
import json
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
from object_detection.utils import visualization_utils as vis_util

MODEL_URL = "http://download.tensorflow.org/models/object_detection/"
LABELS_URL = "https://raw.githubusercontent.com/tensorflow/models/master/research/object_detection/data/mscoco_label_map.pbtxt"
PATH_TO_LABELS = "/tmp/mscoco_label_map.pbtxt"
TMP_DIR = "/tmp"
IMAGE_EXT = [
    ".bmp",
    ".pbm",
    ".pgm",
    ".ppm",
    ".sr",
    ".ras",
    ".jpeg",
    ".jpg",
    ".jpe",
    ".jp2",
    ".tiff",
    ".tif",
    ".png",
]

def parse_dir(dirname):
    fileList = []
    info_json = ""
    for filename in os.listdir(dirname):
        if os.path.isfile(dirname + "/" + filename) is True:
            if filename == "info.json":
                info_json = os.path.join(dirname, filename)
                print(f"File info.json found at {info_json}")
            else:
                _, ext = os.path.splitext(filename)
                if ext in IMAGE_EXT:
                    fileList.append(dirname + "/" + filename)
    if not os.path.isfile(info_json):
        print("ERROR the file info.json could not be found, check cut-video function!")
    return (fileList, info_json)


def prepare_output(info, output_path):
    size = (info["width"], info["height"])
    print(f"Using filename {os.path.basename(info['filename'])}")
    video_path = os.path.join(
        output_path, f"{os.path.basename(info['filename'])}-tagged{info['ext']}"
    )
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    return cv2.VideoWriter(video_path, fourcc, info["fps"], size), video_path


def save_video_info(cap, path, filename, frames):
    info = {}
    name, ext = os.path.splitext(filename)
    info["filename"] = name
    info["ext"] = ext
    info["fps"] = cap.get(cv2.CAP_PROP_FPS)
    info["frame_count"] = frames
    info["width"] = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    info["height"] = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    info["fourcc"] = int(cap.get(cv2.CAP_PROP_FOURCC))
    if info["fourcc"] == 0:
        info["fourcc"] = cv2.VideoWriter_fourcc(*"mp4v")
        info["ext"] = ".mp4"
    info_json = json.dumps(info)
    f = open(path + "/info.json", "w")
    f.write(info_json)
    f.close()
    return info


def download_data(model_name):
    http = urllib3.PoolManager(cert_reqs='CERT_NONE')
    if not os.path.isfile(PATH_TO_LABELS):
        # Downloading label map
        r = http.request("GET", LABELS_URL)
        if r.status == 200:
            f = open(PATH_TO_LABELS, "wb")
            f.write(r.data)
            f.close()
    if not os.path.isdir(model_name):
        # Downloading model
        MODEL_DATA = model_name + ".tar.gz"
        print(f"Dowloading model {MODEL_URL + MODEL_DATA}")
        r = http.request("GET", MODEL_URL + MODEL_DATA)
        if r.status == 200:
            TF_MODEL_PATH = TMP_DIR + "/" + MODEL_DATA
            f = open(TF_MODEL_PATH, "wb")
            f.write(r.data)
            f.close()
            tar_file = tarfile.open(TF_MODEL_PATH)
            for file in tar_file.getmembers():
                file_name = os.path.basename(file.name)
                if "frozen_inference_graph.pb" in file_name:
                    tar_file.extract(file, TMP_DIR)
                    print(f"Extracted file {file.name} on directory {TMP_DIR}")
                    os.remove(TF_MODEL_PATH)
        else:
            raise Exception(f"Invalid model name: {model_name}")


def load_data(model_name):
    print("Loading TensorFlow model...")
    # Load a (frozen) Tensorflow model into memory.
    frozen_graph = os.path.join(
        os.path.join(TMP_DIR, model_name), "frozen_inference_graph.pb"
    )
    print(f"Checking for model in {frozen_graph}")
    if os.path.isfile(frozen_graph):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.compat.v1.gfile.GFile(frozen_graph, "rb") as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name="")
        # Loading label map
        category_index = label_map_util.create_category_index_from_labelmap(
            PATH_TO_LABELS, use_display_name=True
        )
        return detection_graph, category_index
    else:
        raise Exception(f"Cannot find model: {model_name}")


def find_files(path, output_dir):
    imageList = []
    fileList = os.listdir(path)
    for file in fileList:
        _, ext = os.path.splitext(file)
        if ext in IMAGE_EXT:
            imageList.append(path + "/" + file)
        else:
            shutil.copy(f"{path}/{file}", f"{output_dir}/{file}")
    imageList.sort()
    return imageList


def load_tensors():
    # Get handles to input and output tensors
    ops = tf.compat.v1.get_default_graph().get_operations()
    all_tensor_names = {output.name for op in ops for output in op.outputs}
    tensor_dict = {}
    for key in [
        "num_detections",
        "detection_boxes",
        "detection_scores",
        "detection_classes",
        "detection_masks",
    ]:
        tensor_name = key + ":0"
        if tensor_name in all_tensor_names:
            tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(tensor_name)
    if "detection_masks" in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict["detection_boxes"], [0])
        detection_masks = tf.squeeze(tensor_dict["detection_masks"], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict["num_detections"][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(
            detection_masks, [0, 0, 0], [real_num_detection, -1, -1]
        )
    else:
        detection_boxes = None
        detection_masks = None
    return tensor_dict, detection_masks, detection_boxes


def run_inference_for_single_image(
    image, session, tensor_dict, detection_masks, detection_boxes
):
    if "detection_masks" in tensor_dict:
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[1], image.shape[2]
        )
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8
        )
        # Follow the convention by adding back the batch dimension
        tensor_dict["detection_masks"] = tf.expand_dims(detection_masks_reframed, 0)
    image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name("image_tensor:0")
    # Run inference
    output_dict = session.run(tensor_dict, feed_dict={image_tensor: image})
    # all outputs are float32 numpy arrays, so convert types as appropriate
    output_dict["num_detections"] = int(output_dict["num_detections"][0])
    output_dict["detection_classes"] = output_dict["detection_classes"][0].astype(
        np.int64
    )
    output_dict["detection_boxes"] = output_dict["detection_boxes"][0]
    output_dict["detection_scores"] = output_dict["detection_scores"][0]
    if "detection_masks" in output_dict:
        output_dict["detection_masks"] = output_dict["detection_masks"][0]
    return output_dict


def handle(req):
    print(f"Request =======> {req}")

    # Video path
    video_path = "/tmp/output"
    if not os.path.exists(video_path):
        os.mkdir(video_path)

    # Find video filename
    filename = req["video"]
    cap = cv2.VideoCapture(filename)
    idx = 0
    # Read video and save each frame
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        image_path = os.path.join(video_path + "/" + str(idx).zfill(10) + ".jpg")
        cv2.imwrite(image_path, frame)
        idx += 1
    # Saving video infos
    save_video_info(cap, video_path, filename, idx)

    # Check if model and label map exist
    download_data(req["model"])
    # Load model
    detection_graph, category_index = load_data(req["model"])
    with detection_graph.as_default():
        with tf.compat.v1.Session() as sess:
            tensor_dict, detection_masks, detection_boxes = load_tensors()
            # Finding image files in directory
            images_path = video_path
            output_dir = os.path.join(TMP_DIR, "result_video")
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)
            imagesList = find_files(images_path, output_dir)
            # Start detection for each images
            for file_name in imagesList:
                img = cv2.imread(file_name)
                output_dict = run_inference_for_single_image(
                    np.expand_dims(img, axis=0),
                    sess,
                    tensor_dict,
                    detection_masks,
                    detection_boxes,
                )
                # Draw boxes
                vis_util.visualize_boxes_and_labels_on_image_array(
                    img,
                    output_dict["detection_boxes"],
                    output_dict["detection_classes"],
                    output_dict["detection_scores"],
                    category_index,
                    instance_masks=output_dict.get("detection_masks"),
                    use_normalized_coordinates=True,
                    line_thickness=8,
                )
                # Save tagged image
                _, name = os.path.split(file_name)
                name, ext = os.path.splitext(name)
                tagged_image = f"{output_dir}/{name}_tagged{ext}"
                cv2.imwrite(tagged_image, img)

    dirname = output_dir
    frameList, info_json = parse_dir(dirname)
    output_path = os.path.join("/tmp", "RESULT_VIDEO")
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    if frameList and info_json:
        frameList.sort()
        info_txt = open(info_json)
        info = json.loads(info_txt.read())
        print(f"Opened {info_json} for reading")
        if info["frame_count"] == len(frameList):
            print(
                f"Frames expected {len(frameList)}, frames counted {info['frame_count']}!"
            )
            # Prepare video writer
            out, video_outpath = prepare_output(info, output_path)
            print(f"Writing tagged video to {video_outpath}!")
            for f in frameList:
                # Write video
                image_np = cv2.imread(f)
                out.write(image_np)
            out.release()
            if os.path.isfile(video_outpath):
                video_info = os.stat(video_outpath)
                if video_info.st_size > 0:
                    print(f"Created file {video_outpath}, size is {video_info.st_size}")
                else:
                    print(
                        f"Something went wrong file {video_outpath}, is zero bytes long!"
                    )
            else:
                print(f"File {video_outpath} does not exists, cv2.VideoWriter failed!")
            return {"video_out": video_outpath}
        else:
            print(
                f"ERROR Frames expected {len(frameList)}, frames counted {info['frame_count']}!"
            )
    else:
        print(f"ERROR cannot find frameList or info_json in directory {dirname}")




if __name__ == "__main__":
    handle({
        "model"  : "ssdlite_mobilenet_v2_coco_2018_05_09",
        "images" : "/tmp/output",
    })
