# TFDetection

TFDetection is a function that use TensorFlow to detect objects.

TFDetection can use models from [Tensorflow Models Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md) and you can also create your own model using [this tutorial](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/defining_your_own_model.md).

**Input:**
> images
  - Directory containing images to be tagged accepted by OpenCV see [here](https://docs.opencv.org/4.1.1/d4/da8/group__imgcodecs.html#ga288b8b3da0892bd651fce07b3bbd3a56)

**Output:**
> tagged_images 
  - Directory containing tagged images from input
